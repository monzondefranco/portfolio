import React from 'react'

const titleText = '< Hello World />';

function Landing() {
    return (
        <div>
            <h1> {titleText} </h1>
            <p>I&apos;m a Web Developer, passionate about what I do, and always learning something new.</p>
            <img src={require("./avatar.svg")} />


            
                <h2>
                Hi, I&apos;m Franco. Pleasure to meet you.
                </h2>
                <p>
                Coming from Buenos Aires, Argentina, I started my journey as a
                developer in 2019. I have special interest in front-end, 
                where I have worked most of my projects and potentialized
                my skills. A fan of learning and interacting with new technologies,
                these are the skills I have learned and improved during my journey.
                </p>
            
            
        </div>
    )
}

export default Landing
