import React from 'react';
import './App.css';
import Landing from './pages/landing';
import Header from './components/header';


function App() {
  return (
    <div className="App">
      <Header />
      <Landing />
    </div>
  );
}

export default App;
