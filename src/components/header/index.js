import React from 'react'
import styled from 'styled-components'
import { DefaultButton } from '../button'


const Container = styled.div `
    align-items: center;
    background: white;
    display: flex;
    height: 5rem;
    justify-content: space-between;
    margin: 0 auto;
    margin-bottom: 1.45rem;
    max-width: 80rem;
    padding: 0 0.5rem;
    padding-top: 3rem;
    width: 100%;
    zindex: 3;
    @media (min-width: 1024px) {
        padding-top: 2.5rem;
    }
`;

const Menu = styled.ul`
    display: none;
    list-style: none;
    margin: 0;
    width: 16rem;
    @media (min-width: 1024px) {
        align-items: center;
        display: flex;
        justify-content: space-around;
    }
`;

const MenuItem = styled.li`
    color: #00343d;
    font-size: 0.875rem;
    margin: 0;
    text-decoration: none;
    &:hover {
    color: #0bd8a2;
    cursor: pointer;
    }
`;

const ResponsiveButton = styled.button`
    background: transparent;
    border: none;
    cursor: pointer;
    display: block;
    font-size: 1.5rem;
    z-index: 3;
    @media (min-width: 1024px) {
        display: none;
    }
`;

function Header() {
    return (
        <Container>
            <img src={require('./imagen-net.png')} style={{ margin: 0 }} alt="" width="100px"/>
            
            <Menu>
                <MenuItem>About Me</MenuItem>
                <MenuItem>
                    <DefaultButton value="Get in touch" />
                </MenuItem>
                
            </Menu>
            
        </Container> 
    )
}

export default Header
